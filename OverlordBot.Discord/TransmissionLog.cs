﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Text;

namespace RurouniJones.OverlordBot.Discord
{
    public abstract class TransmissionLog
    {
        public ulong DiscordServer { get; set; }
        public ulong DiscordChannel { get; set; }
        public string ServerShortName { get; set; }
        public string ControllerName { get; set; }
        public double Frequency { get; set; }
        public string Modulation { get; set; }
        public string ControllerCallsign { get; set; }
        public List<string> PlayersOnFrequency { get; set; }

        protected StringBuilder AddHeader(StringBuilder sb)
        {
            sb.AppendLine()
                .AppendLine("```yaml")
                .Append($"Controller Name: {ServerShortName} {ControllerName}");
            if (ControllerCallsign != null)
            {
                sb.AppendLine($" (Callsign: {ControllerCallsign})");
            }
            else
            {
                sb.AppendLine();
            }

            sb.AppendLine($"Frequency: {Frequency} {Modulation}")
                .AppendLine($"  Players: {string.Join(", ", PlayersOnFrequency)}");
            return sb;
        }

        public abstract string ToDiscordLog();
    }
}