﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using NLog;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RurouniJones.OverlordBot.GrpcDatastore
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly DataSource _dataSource;
        public PlayerRepository(DataSource dataSource) 
        {
            _dataSource = dataSource;
        }

        public async Task<Unit> FindByCallsign(string groupName, int flight, int element)
        {
            var matches = _dataSource.Units.Values.Where(p => PilotOrCallsignMatches(p, groupName, flight, element) &&
            !string.IsNullOrEmpty(p.Pilot)).ToList();
            if (matches.Count > 1)
            {
                foreach(Unit unit in matches)
                    _logger.Warn(unit);
                throw new DuplicateCallsignException(groupName, flight, element);
            } else
            {
                return matches.FirstOrDefault();
            }
        }

        public async Task<Unit> FindByCallsignAndCoalition(string groupName, int flight, int element, int coalition)
        {
            var matches = _dataSource.Units.Values.Where(p => PilotOrCallsignMatches(p, groupName, flight, element) &&
                                               p.Coalition == coalition &&
                                               !string.IsNullOrEmpty(p.Pilot)).ToList();
            if (matches.Count > 1)
            {
                foreach (Unit unit in matches)
                    _logger.Warn(unit);
                throw new DuplicateCallsignException(groupName, flight, element);
            }
            return matches.FirstOrDefault();
        }

        public async Task<Unit> FindById(string id)
        {
            return _dataSource.Units.ContainsKey(id) ? _dataSource.Units[id] : null;
        }

        private static bool PilotOrCallsignMatches(Unit u, string groupName, int flight, int element)
        {
            return u.Pilot.ToLower().Contains($"{groupName.ToLower()} {flight}-{element}") ||
                   u.Pilot.ToLower().Contains($"{groupName.ToLower()} {flight}{element}") ||
                   u.Pilot.ToLower().Contains($"{groupName.ToLower()}{flight}-{element}") ||
                   u.Pilot.ToLower().Contains($"{groupName.ToLower()}{flight}{element}") ||
                   u.Callsign.ToLower().Contains($"{groupName.ToLower()} {flight}{element}") ||
                   u.Callsign.ToLower().Contains($"{groupName.ToLower()}{flight}{element}") ||
                   u.Callsign.ToLower().Contains($"{groupName.ToLower()} {flight}-{element}") ||
                   u.Callsign.ToLower().Contains($"{groupName.ToLower()}{flight}-{element}");
        }
    }
}
