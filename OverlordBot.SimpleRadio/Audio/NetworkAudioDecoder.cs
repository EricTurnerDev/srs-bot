﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using Concentus.Structs;
using NAudio.Wave;

namespace RurouniJones.OverlordBot.SimpleRadio.Audio
{
    public class NetworkAudioDecoder
    {
        private const int InputSampleRate = 16000;
        private const int Channels = 1; // Mono audio
        private const int InputAudioLength = 40; // ms
        private const int FrameSize = InputSampleRate / 1000 * InputAudioLength;

        private readonly BufferedWaveProvider _audioBuffer;

        private readonly OpusDecoder _decoder = new(InputSampleRate, Channels);

        public NetworkAudioDecoder(BufferedWaveProvider audioBuffer)
        {
            _audioBuffer = audioBuffer;
        }

        public void Decode(byte[] encodedOpusAudio)
        {
            var pcmAudioShort = new short[FrameSize];
            _decoder.Decode(encodedOpusAudio, 0, encodedOpusAudio.Length, pcmAudioShort, 0, FrameSize);

            var pcmAudio = ShortArrayToByteArray(pcmAudioShort);

            _audioBuffer.AddSamples(pcmAudio, 0, pcmAudio.Length);
        }

        private static byte[] ShortArrayToByteArray(short[] shortArray)
        {
            var byteArray = new byte[shortArray.Length * sizeof(short)];
            Buffer.BlockCopy(shortArray, 0, byteArray, 0, byteArray.Length);
            return byteArray;
        }
    }
}

