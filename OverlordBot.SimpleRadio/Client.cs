﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using NAudio.Wave;
using NLog;
using RurouniJones.OverlordBot.SimpleRadio.Models;
using RurouniJones.OverlordBot.SimpleRadio.Network;
using RurouniJones.OverlordBot.SimpleRadio.Network.Util;

namespace RurouniJones.OverlordBot.SimpleRadio
{
    public class Client
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        
        /// <summary>
        ///     Version of the SRS protocol that we support.
        /// </summary>
        public const string Version = "1.9.0.0";

        /// <summary>
        /// Human readable name for our SRS client instance. Appears on the Admin list
        /// in the SRS server and the UIs of other clients when we are transmitting.
        /// </summary>
        private readonly ClientInformation _clientInformation;

        /// <summary>
        /// Radio information that we need to send to the SRS server after it has accepted
        /// our AWACS authentication request.
        /// </summary>
        private readonly RadioInformation _radioInformation;

        private readonly string _password;
        private readonly double _frequency;
        private readonly ConcurrentDictionary<string, string> _playersOnFrequency;
        private readonly BufferedWaveProvider _audioBuffer;
        private readonly ConcurrentQueue<Queue<(byte[], TimeSpan)>> _transmissionQueue;
        private readonly RadioInformation.Radio.Modulations _modulation;


        private readonly IPEndPoint _srsEndpoint;
        public bool IsConnected;

        public Client(string hostname, int port, string clientName, string password, double frequency, string modulation,
            BufferedWaveProvider audioBuffer, ConcurrentQueue<Queue<(byte[], TimeSpan)>> transmissionQueue, ConcurrentDictionary<string, string> playersOnFrequency)
        {
            // Ensure we get an IPv4 address in case the host resolves to both IPv6 and IPv4
            var ipAddress = Dns.GetHostAddresses(hostname).FirstOrDefault(xa => xa.AddressFamily == AddressFamily.InterNetwork);
            if (ipAddress == null)
            {
                throw new Exception($"Could not determine IPv4 address for {hostname}");
            }

            _srsEndpoint = new IPEndPoint(ipAddress, port);
            _password = password;
            _frequency = frequency;
            _playersOnFrequency = playersOnFrequency;
            _audioBuffer = audioBuffer;
            _transmissionQueue = transmissionQueue;
            Enum.TryParse(modulation, true, out _modulation);
            var guid = ShortGuid.NewGuid();

            _clientInformation = new ClientInformation
            {
                Coalition = 0,
                Name = $"OverlordBot {clientName}",
                Guid = guid.ToString()
            };

            _radioInformation = new RadioInformation
            {
                Radios = new List<RadioInformation.Radio>
                    {new() {Frequency = frequency * 1000000, Modulation = _modulation}}
            };

        }

        public async Task ConnectAsync(CancellationToken controllerCancellationToken)
        {
            // For ss long as the service itself is running we will always try to reconnect to SRS.
            while (!controllerCancellationToken.IsCancellationRequested)
            {
                // This local cancellation source and token is specific to the various processing loops for this client
                // and this client only. By calling this whenever we have an error in one of the processing loops we
                // make sure that we shut them all down before starting fresh with a new connection attempt.
                var clientCancellationSource = CancellationTokenSource.CreateLinkedTokenSource(controllerCancellationToken);
                var clientCancellationToken = clientCancellationSource.Token;
                
                _audioBuffer.ClearBuffer();

                _logger.Info("Connecting");
                // Avoid port hammering.
                Thread.Sleep(1000);

                var processingTasks = new Dictionary<string, Task>();

                // Initial non-loop actions
                var dataClient = new DataClient(_clientInformation, _radioInformation, _playersOnFrequency);
                var audioClient = new AudioClient(_clientInformation, _radioInformation, _transmissionQueue);
                IsConnected = await ConnectDataClientAsync(dataClient, clientCancellationToken);

                if (!IsConnected) continue;

                _logger.Info("Connected");
                _logger.Info("Client tasks starting");

                processingTasks.Add("DataClient client/server updates", dataClient.StartClientServerUpdatesAsync(clientCancellationToken));

                // There is no return for this. Confirmation of connection or not will be done in the
                // ClientServerUpdatesAsync loop
                await dataClient.SendExternalAwacsRequest(_password);

                processingTasks.Add("AudioClient ping", audioClient.StartAudioPingAsync(_srsEndpoint, clientCancellationToken));
                processingTasks.Add("AudioClient timeout checker", audioClient.StartCheckingAudioTimeoutAsync(clientCancellationToken));
                processingTasks.Add("AudioClient audio data receiver", audioClient.StartReceivingAudioDataAsync(clientCancellationToken));
                processingTasks.Add("AudioClient audio data decoder", audioClient.StartDecodingAudioDataAsync(_frequency, _modulation, _audioBuffer, clientCancellationToken));
                processingTasks.Add("AudioClient transmission end checker", audioClient.StartCheckingForEndOfTransmissions(_audioBuffer, clientCancellationToken));
                processingTasks.Add("AudioClient transmission queue checker", audioClient.StartCheckingTransmissionsQueue(_srsEndpoint, clientCancellationToken));

                _logger.Info("Client tasks started");

                // If any of the processing tasks have finished then something has gone wrong or we are shutting down
                // the service. Either way start the processing stopping all the processing tasks.
                await Task.WhenAny(processingTasks.Values);
                IsConnected = false;

                if (!clientCancellationToken.IsCancellationRequested)
                {
                    _logger.Info("Client unexpectedly disconnected");
                    foreach (var (key, value) in processingTasks)
                    {
                        _logger.Debug($"{key}: {value.Status}, {value.Exception?.Message}");
                    }
                }
                // Only cancel the rest of the tasks if we are not in a global shutdown which will already have
                // cancelled them leading to an exception if we try again.
                if(!controllerCancellationToken.IsCancellationRequested) clientCancellationSource.Cancel();
                _logger.Info("Client tasks stopping");

                // Wait for all of the processing tasks to finish 
                await Task.WhenAll(processingTasks.Values);
            }
        }

        private async Task<bool> ConnectDataClientAsync(DataClient dataClient, CancellationToken cancellationToken)
        {
            var isConnectionSuccess = await dataClient.ConnectAsync(_srsEndpoint, cancellationToken);
            if (!isConnectionSuccess) return false;

            var isSyncSuccess = await dataClient.SendInitialSync();
            return isSyncSuccess;
        }
    }
}