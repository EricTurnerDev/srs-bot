﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Tests
{
    [TestClass]
    public class GameUnitTests
    {
        private static readonly Unit baseUnit = new() {
                Id = "1",
                Altitude = 1000,
                Coalition = 1,
                Group = "Group 1",
                Heading = 90,
                Location = new Geo.Geometries.Point(40, 40),
                Name = "Name 1",
                Pilot = "Pilot 1",
                Speed = 300
            };

        [TestMethod]
        public void Equality_WhenAltitudeIsDifferent_IsEqual()
        {
            var newUnit = baseUnit with {Altitude = 1100};
            Assert.AreEqual(baseUnit, newUnit);
            Assert.IsTrue(baseUnit == newUnit);
        }

        [TestMethod]
        public void Equality_WhenHeadingIsDifferent_IsEqual()
        {
            var newUnit = baseUnit with {Heading = 270};
            Assert.AreEqual(baseUnit, newUnit); 
            Assert.IsTrue(baseUnit == newUnit);
        }

        [TestMethod]
        public void Equality_WhenLocationIsDifferent_IsEqual()
        {
            var newUnit = baseUnit with {Location = new Geo.Geometries.Point(41, 41)};
            Assert.AreEqual(baseUnit, newUnit);
            Assert.IsTrue(baseUnit == newUnit);
        }
        
        [TestMethod]
        public void Equality_WhenSpeedIsDifferent_IsEqual()
        {
            var newUnit = baseUnit with {Speed = 500};
            Assert.AreEqual(baseUnit, newUnit);
            Assert.IsTrue(baseUnit == newUnit);
        }

        [TestMethod]
        public void Equality_WhenIdIsDifferent_IsNotEqual()
        {
            var newUnit = baseUnit with {Id = "2"};
            Assert.AreNotEqual(baseUnit, newUnit);
            Assert.IsFalse(baseUnit == newUnit);
        }

        [TestMethod]
        public void Equality_WhenCoalitionIsDifferent_IsNotEqual()
        {
            var newUnit = baseUnit with {Coalition = 2};
            Assert.AreNotEqual(baseUnit, newUnit);
            Assert.IsFalse(baseUnit == newUnit);
        }

        [TestMethod]
        public void Equality_WhenGroupIsDifferent_IsNotEqual()
        {
            var newUnit = baseUnit with {Group = "Group 2"};
            Assert.AreNotEqual(baseUnit, newUnit);
            Assert.IsFalse(baseUnit == newUnit);
        }

        [TestMethod]
        public void Equality_WhenNameIsDifferent_IsNotEqual()
        {
            var newUnit = baseUnit with {Name = "Name 2"};
            Assert.AreNotEqual(baseUnit, newUnit);
            Assert.IsFalse(baseUnit == newUnit);
        }

        [TestMethod]
        public void Equality_WhenPilotIsDifferent_IsNotEqual()
        {
            var newUnit = baseUnit with {Pilot = "Pilot 2"};
            Assert.AreNotEqual(baseUnit, newUnit);
            Assert.IsFalse(baseUnit == newUnit);
        }
    }
}
