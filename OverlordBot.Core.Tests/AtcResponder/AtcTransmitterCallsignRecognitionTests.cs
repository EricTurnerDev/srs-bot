﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Tests.Mocks;

namespace RurouniJones.OverlordBot.Core.Tests.AtcResponder
{
    /// <summary>
    /// Verify the Responder handles incomplete transmitter callsigns gracefully.
    ///
    /// We are only testing situations where the callsign is not fully recognizable/
    /// Transmissions with a fully recognizable callsign will be handled in happy-path tests
    /// </summary>
    [TestClass]
    public class AtcTransmitterCallsignRecognitionTests
    {
        private const string ServerShortName = "Test";
        private const string AirfieldName = "kutaisi";
        private static readonly string PronouncedAirfieldName = Cognitive.SpeechOutput.AirbasePronouncer.PronounceAirbase(AirfieldName);
        private static HashSet<AirfieldStatus> airfields = new();

        [TestMethod]
        public async Task WhenAPlayerCallsAtcWithPartiallyRecognizableCallsign_ThenReturnsResponse()
        {
            var controller = new Atc.AtcResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), airfields, ServerShortName);
            var player = new ITransmission.Player("Dolt", -1, -1);
            var airfield = new ITransmission.Airfield(AirfieldName,"ground");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, airfield);

            var expectedResponse = $"dolt x x, {PronouncedAirfieldName} ground, i could not fully recognize your callsign";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }

        [TestMethod]
        public async Task WhenAPlayerCallsAtcWithUnrecognizableCallsign_ThenReturnsResponse()
        {
            var controller = new Atc.AtcResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), airfields, ServerShortName);
            var player = new ITransmission.Player(null, -1, -1);
            var airfield = new ITransmission.Airfield(AirfieldName,"ground");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, player, airfield);
            
            var expectedResponse = $"last transmitter, {PronouncedAirfieldName} ground, i could not recognize your callsign";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }

        [TestMethod]
        public async Task WhenAPlayerCallsAtcWithNoTransmitter_ThenReturnsResponse()
        {
            var controller = new Atc.AtcResponder(new MockPlayerRepository(), new MockUnitRepository(), new MockAirfieldRepository(), airfields, ServerShortName);
            var airfield = new ITransmission.Airfield(AirfieldName,"ground");
            var transmission = new BasicTransmission(null, ITransmission.Intents.RadioCheck, null, airfield);
            
            var expectedResponse = $"last transmitter, {PronouncedAirfieldName} ground, i could not recognize your callsign";
            var reply = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, reply.ToSpeech());
        }
    }
}
