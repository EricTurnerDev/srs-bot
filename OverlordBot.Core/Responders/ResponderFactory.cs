﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Microsoft.Extensions.DependencyInjection;
using RurouniJones.OverlordBot.Datastore;
using System;
using System.Collections.Generic;

namespace RurouniJones.OverlordBot.Core.Responders
{
    public class ResponderFactory
    {
        private readonly IServiceProvider _serviceProvider;
        public ResponderFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IResponder CreateResponder(Configuration.ControllerConfiguration.ControllerType type,
            IPlayerRepository playerRepository,           
            IUnitRepository unitRepository,
            IAirfieldRepository airfieldRepository,
            HashSet<AirfieldStatus> airfields,
            bool restrictedLineOfSight,
            string awacsPilot,
            string awacsCallsign,
            string serverShortName)
        {
            if(type == Configuration.ControllerConfiguration.ControllerType.Awacs)
            {
                return ActivatorUtilities.CreateInstance<Awacs.AwacsResponder>(_serviceProvider, playerRepository, unitRepository, airfieldRepository, restrictedLineOfSight, awacsPilot, awacsCallsign ?? "", serverShortName);

            }
            else if(type == Configuration.ControllerConfiguration.ControllerType.Atc)
            {
                return ActivatorUtilities.CreateInstance<Atc.AtcResponder>(_serviceProvider, playerRepository, airfieldRepository, unitRepository, airfields, serverShortName);

            }
            else
            {
                return _serviceProvider.GetService<MuteResponder>();
            }
        }
    }
}
